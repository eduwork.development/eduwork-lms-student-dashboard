import { login } from "@/src/api/authApi"
import Loader from "@/src/components/all/loader"
import { app } from "@/src/config/app"
import { AppDispatch } from "@/src/store"
import { authActions, getAuthLoading, getAuthUser } from "@/src/store/authSlice"
import { FormEvent, useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { toast } from "react-toastify"

export default function Login() {
  const dispatch: AppDispatch = useDispatch()
  const [showPassword, setShowPassword] = useState<Boolean>(false)
  const auth = useSelector(getAuthUser)
  const [u, setU] = useState('')
  const [p, setP] = useState('')
  const loading = useSelector(getAuthLoading).includes("login_action")

  const _handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    dispatch(authActions.addLoad("login_action"))
    if (!loading) {
      login({
        payload: {
          email: u,
          password: p
        }
      }).then((data: any) => {
        if (data.errors) {
          throw {
            message: 'Login Gagal'
          }
        } else {
          if (data.data.user.level === 'student') {
            dispatch(authActions.login({...data.data}))
            toast("Login berhasil, Mengalihkan...", {
              type: 'success'
            })
          } else {
            throw {
              message: 'Only accept student account.'
            }
          }
        }
        dispatch(authActions.removeLoad("login_action"))
      }).catch(err => {
        toast(err.message, {
          type: 'error'
        })
        dispatch(authActions.removeLoad("login_action"))
      })
    }
  }

  useEffect(() => {
    setU(u)
    setP(p)
  }, [u,p])

  return (
    <div className="bg-blue-50 w-[100vw] h-[100vh]">
      <div className="fixed w-full h-full md:w-[671px] md:h-[588px] inset-1/2 -translate-y-1/2 -translate-x-1/2">
        <div className="relative bg-white rounded-lg shadow-lg py-6 md:py-12 px-7 md:px-24">
          <div className="flex items-start justify-between rounded-t mb-8">
            <h3 className="text-2xl font-semibold text-gray-900">Login</h3>
            {/* <button type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center" data-modal-toggle="modalLogin">
              <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
              </svg>
              <span className="sr-only">Close modal</span>
            </button> */}
          </div>
          <div className="space-y-6">
            <p className="text-sm md:text-base leading-relaxed text-gray-500">
              Faster login or register use your social account
            </p>
            <a href={`${app.APP_URL}/social/redirect/google?permalink=${window.location.href}`} className="bg-[#ff0000] flex gap-2 items-center w-full justify-center rounded-xl py-3">
              <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFRSURBVHgB7ZRNSgMxFMeT+oW6EFyLrhRBsFW8gdZreARxr5cQRA+gJ7DgERQFnSqu/AAXblxXxREn/oIpvoZMJ1O67B9+zEzeyz+ZyXuj1EAB6aIEY8wIlyoswSR8QBMSrbVRvQrjTbiCb9OpDJpQV2XFpAqcmDjtlTU/8gxSuIFTd03d+BMsljGue8bnUAPt4to9n8FcGWM7MRHGtzCs+iG3o0x8ipXA4hVHYaX5u6qp//K8h8SLb8OOWCwTsRasU55veeYT4v41UMdTMKvCsrnjcqDiJbTE/UzMqwul8JUbxWxNNEzom0/DvGBXHP5z18MnOGr+OrKti7wJjI/Bi8g9UEUK1PkdrHp1XoVLkfMDCypGJB57C6Su/hums0Pb2lexInkIDk2cGqaXRmPShttp5hna5wfYMl0qKuZ/bnOWwVaO7YNPeIRr+uBdDVRWvw4NC6VT6y2CAAAAAElFTkSuQmCC" alt="sign google" />
              <span className="text-white font-medium">Login with google</span>
            </a>
            <div className="flex items-center gap-4">
              <div className="h-[1px] bg-gray-300 w-full"></div>
              <p>Or</p>
              <div className="h-[1px] bg-gray-300 w-full"></div>
            </div>
            <form className="flex flex-col gap-4" onSubmit={_handleSubmit} method="post">
              <input type="hidden" name="type" value="login" />
              <div className="flex">
                <input type="text" name="phone" defaultValue={u} onChange={(e) => setU(e.target.value)} className="rounded-xl bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm py-3 px-4" placeholder="Phone Number / Email" />
              </div>
              <div className="flex">
                <input type={showPassword ? 'text':'password'} defaultValue={p} onChange={(e) => setP(e.target.value)} name="password" className="rounded-none rounded-l-xl bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm py-3 px-4" placeholder="Password" id="m-password" />
                <span id="eye" onClick={() => setShowPassword(!showPassword)} className="cursor-pointer inline-flex items-center px-4 text-sm text-gray-900 bg-gray-200 rounded-r-xl border border-l-0 border-gray-300 hide">
                  {
                    !showPassword &&
                    <img id="eye-hide" className="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFzSURBVHgBlZE/SEJRGMW9Tw2a2qKhpXCLGvQJjUFDU1MIhkMU4h9eg0XQIjQGDa2hIi8KIkKzBp2agtoUwpZwaBAcWqNBfX/6XfGJb4m8cO797uGe8873PU8ymcym0+lFzwRLEUI8mKZZnUQo5KZp2pJhGCXbtmOYrEHFwAzogHKv17vXdb3tEjnCfr9fRTTH9RGDFnUAbFF/w2VyudyVfOuVG30dETFqWVZGUZTVbrd7WCwWb+v1eikcDr8hivIsEgqFLLhnkUqlDiDPpRjRCc4VSp2zw10vFAoVTG/gtoeh4grb/iirEFP5fL5JuYvRCl/dkzz159gcsgqxLsZEgwkOhZtgPpFILMvenDcYXHobjcarqqqyeRUEgsFgE+6D7F/084JAh1+XngjOMMyOpkfuY8hTIpmcZagW5zSia/BEojv601wjH3QYjy/4fL4dHm9wnQU/sme/39/mX9UQRpjqu0v015KGpKiByL9FjpCosV8Eu6+EP/9hhQAAAABJRU5ErkJggg==" alt="" />
                  }
                  {
                    showPassword &&
                    <img id="eye-show" className="w-[18px] h-[21px]" src={`${app.APP_URL}/images/eye-show.png`} alt="" />
                  }
                </span>
              </div>
              {/* {
                error &&
                <div className="text-red-500 font-normal border border-red-500 rounded-lg bg-red-200 p-2" role="alert">
                  <strong>{auth.message.text}</strong>
                </div>
              } */}
              <div className="flex items-center justify-between w-full text-xs">
                <a href={`${app.APP_URL}/password/reset`} className="font-medium text-primary self-end text-xs">Forgot Password?</a>
                <h1 className="space-x-2">
                  <span>Don’t have an account?</span>
                  <a href={`${app.APP_URL}/m`} className="font-medium text-primary underline">Register Now</a>
                </h1>
              </div>
              {
                loading ?
                <button className="bg-gray-200 text-gray flex gap-2 items-center w-full justify-center rounded-xl py-3 !cursor-default" type="button">
                  <Loader className="text-gray-500" />
                  {/* <span className="text-white font-medium">Login</span> */}
                </button>:
                <button className="bg-primary-main flex gap-2 items-center w-full justify-center rounded-xl py-3" type="submit">
                  <span className="text-white font-medium">Login</span>
                </button>
              }
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}