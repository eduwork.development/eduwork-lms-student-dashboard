import { openNextProgressApi, pickTaskApi } from "@/src/api/registrantApi"
import Loader, { LoaderPulse } from "@/src/components/all/loader"
import Button, { ButtonColorType } from "@/src/components/atoms/button"
import Modal from "@/src/components/molecules/modal"
import Skeleton from "@/src/components/molecules/skeleton"
import { ShowReviewModal } from "@/src/components/organisms/material"
import { app } from "@/src/config/app"
import { AppDispatch, RootState } from "@/src/store"
import { bannerActions } from "@/src/store/bannerSlice"
import { classActions, classActiveCurriculum, classCategoryById, classCurriculumByCategoryId, classCurriculumById, classLoading, classTaskByCurriculumId, selectClassUserReviewCurriculum, selectNextCurriculum, selectNextCurriculumInCategory } from "@/src/store/classSlice"
import { canAccessCurriculum, getRegistrantByClassId, getRegistrantLastProgress, registrantLoading, selectRegistrantProgressByCurriculumId, selectRegistrantTaskAllByCurriculumId } from "@/src/store/registrantSlice"
import { ReactNode, memo, useEffect, useState } from "react"
import { createPortal } from "react-dom"
import { HiArrowRight, HiOutlineLockOpen } from "react-icons/hi2"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate, useParams } from "react-router-dom"
import { toast } from "react-toastify"

type BtnNextType = '' | 'next' | 'pick_task' | 'open_curriculum' | 'open_category' | 'open_upgrade' | 'open_consultation'

type NextBtnCurriculumProps = {}
const NextBtnCurriculum = ({}:NextBtnCurriculumProps) => {
  const {classId} = useParams()
  const dispatch: AppDispatch = useDispatch()
  const web = useNavigate()
  const [dismissReviewModal,setDismissedReviewModal] = useState(true)
  const loadingReviewCurriculum = useSelector(registrantLoading).includes('fetch_existing_review')
  const loadingPickTask = useSelector(registrantLoading).includes('pick_task')
  const loading = useSelector(classLoading).includes('fetch_curriculum')
  const lastProgress = useSelector(getRegistrantLastProgress)
  const [mountedLoading,setMountedLoading] = useState(true)
  const curriculumId = useSelector(classActiveCurriculum)
  const registrant = useSelector((state: RootState) => getRegistrantByClassId(state,classId as unknown as number))
  const activeCurriculum = useSelector((state: RootState) => classCurriculumById(state,curriculumId as unknown as number))
  const currentProgress = useSelector((state: RootState) => selectRegistrantProgressByCurriculumId(state,curriculumId as unknown as number))
  const existingReview = useSelector((state: RootState) => selectClassUserReviewCurriculum(state, {
    curriculumId: curriculumId as unknown as number,
    studentId: registrant?.student_id as unknown as number
  }))
  const existingReviewid = existingReview?.id
  const nextCurriculum = useSelector((state: RootState) => selectNextCurriculum(state, classId as unknown as number))
  const nextCurriculumCategory = useSelector((state:RootState) => classCategoryById(state,nextCurriculum?.curriculum_category_id as unknown as number))
  const currentCurriculumCategory = useSelector((state:RootState) => classCategoryById(state,activeCurriculum?.curriculum_category_id as unknown as number))
  const currentCategoryAccessible = useSelector((state: RootState) => canAccessCurriculum(state,{
    curriculumCategoryId: activeCurriculum?.curriculum_category_id as unknown as number,
    registrantId: registrant?.id as unknown as number
  }))
  const categoryAccessible = useSelector((state: RootState) => canAccessCurriculum(state,{
    curriculumCategoryId: nextCurriculumCategory?.id as unknown as number,
    registrantId: registrant?.id as unknown as number
  }))
  const currentClassTasks = useSelector((state:RootState) => classTaskByCurriculumId(state,activeCurriculum?.id as unknown as number))
  const currentProgressTask = useSelector((state:RootState) => selectRegistrantTaskAllByCurriculumId(state,activeCurriculum?.id as unknown as number))
  const nextCurriculumInCategory = useSelector((state:RootState) => selectNextCurriculumInCategory(state,{
    categoryId: currentCurriculumCategory?.id as unknown as number,
    activeCurriculumId: activeCurriculum?.id as unknown as number
  }))
  const nextProgressOutCategory = useSelector((state:RootState) => selectRegistrantProgressByCurriculumId(state,nextCurriculum?.id as unknown as number))
  const nextProgressInCategory = useSelector((state:RootState) => selectRegistrantProgressByCurriculumId(state,nextCurriculumInCategory?.id as unknown as number))

  const openProgressLoading = useSelector(registrantLoading).includes("open_next_progress")
  const [openSectionModal,setOpenSectionModal] = useState<boolean>(false)

  let btnColor: ButtonColorType = 'light'
  let btnText: string | ReactNode = ''
  let btnType: BtnNextType = ''

  const isInTheLastCurriculum = () => {
    return (currentCurriculumCategory?.id != nextCurriculumCategory?.id) && !nextCurriculumInCategory
  }

  const hasNextCategory = () => {
    return nextCurriculumCategory
  }

  const hasNextProgress = () => {
    return isInTheLastCurriculum() ? nextProgressOutCategory: nextProgressInCategory
  }

  const currentHasTaskPicked = () => {
    return (currentClassTasks.length > 0 && currentProgressTask.length > 0) || (currentClassTasks.length === 0)
  }

  const currentHasTaskNotPicked = () => {
    return (currentClassTasks.length > 0 && currentProgressTask.length == 0)
  }

  const hasNextCurriculum = () => {
    return nextCurriculum
  }  

  const _openNextProgress = () => {
    setOpenSectionModal(false)
    dispatch(openNextProgressApi({
      payload: {
        registrant_id: registrant?.id as unknown as number
      }
    })).then((data) => {
      const nextId = isInTheLastCurriculum() ? nextCurriculum?.id: nextCurriculumInCategory?.id
      dispatch(classActions.setActiveCurriculum(nextId)) 
      web(`/my-class/${classId}/material/${nextId}/task`)
      toast("Chapter opened.", {
        type: 'success'
      })
    }).catch(err => {
      toast("Failed to open chapter.", {
        type: 'error'
      })
      setOpenSectionModal(true)
    })
  }

  const _handleNextBtn = (type: BtnNextType) => {
    if (['next'].includes(type)) {
      const nextId = isInTheLastCurriculum() ? nextCurriculum?.id: nextCurriculumInCategory?.id
      dispatch(classActions.setActiveCurriculum(nextId)) 
      web(`/my-class/${classId}/material/${nextId}/task`)
    } else if (['open_curriculum','open_category'].includes(type)) {
      if (!existingReview) {
        setDismissedReviewModal(!dismissReviewModal)      
      } else {
        if (isInTheLastCurriculum() && type == 'open_category') {
          setOpenSectionModal(true)
        } else {
          _openNextProgress()
        }
      }
    } else if(['open_category'].includes(type)) {
      // setOpenSectionModal(true)
    } else if (['open_consultation','open_upgrade'].includes(type)) {
      if (type == 'open_upgrade') {
        dispatch(bannerActions.addBannerData(`${app.APP_URL}/templates/v1/landing/lock-main-class.png`))
      }
      dispatch(bannerActions.addBanner("lock_curriculum"))
    } else if(['pick_task'].includes(type)) {
      dispatch(pickTaskApi({
        payload: {
          registrant_id: registrant?.id as unknown as number,
          curriculum_id: curriculumId as unknown as number
        }
      })).then((data) => {
        const result = data as {data: {tasks: Array<Record<any,any>>}}
        toast(`Berhasil mendapatkan ${result.data.tasks.length} tasks.`, {
          type: 'success'
        })
      }).catch(err => {
        toast(err.message, {
          type: 'error'
        })
      })
    }
  }

  const _handleOpenNewSection = () => {
    _openNextProgress()
  }

  
  if (isInTheLastCurriculum() && hasNextCategory() && !categoryAccessible && currentCategoryAccessible) {
    btnText = "Next Section"
    btnColor = 'success'
    if (['unprocessed','confirm',''].includes(registrant?.status ?? '')) {
      btnType = 'open_consultation'
    } else if(['incubation'].includes(registrant?.status ?? '')) {
      btnType = 'open_upgrade'
    }
  } else if(currentCategoryAccessible) {
    if(hasNextProgress()) {
      btnText = <HiArrowRight />
      btnColor = 'info'
      btnType = 'next'
    } else if (currentHasTaskNotPicked()) {
      btnText = "Ambil Task"
      btnColor = 'info'
      btnType = 'pick_task'
    } else if (isInTheLastCurriculum() && hasNextCategory()) {
      btnText = "Next Section"
      btnColor = 'warning'
      btnType = 'open_category'
    } else if (hasNextCurriculum()) {
      btnText = "Next Chapter"
      btnColor = 'success'
      btnType = 'open_curriculum'
    }
  } else {
    btnText = <div className="flex items-center space-x-2"><HiOutlineLockOpen /><div>Buka Materi</div></div>
    btnColor = 'primary'
    if (['unprocessed','confirm',''].includes(registrant?.status ?? '')) {
      btnType = 'open_consultation'
    } else if(['incubation'].includes(registrant?.status ?? '')) {
      btnType = 'open_upgrade'
    }
  }

  useEffect(() => {
    setMountedLoading(false)
  }, [])

  return (
    <div>
      {
        (loadingReviewCurriculum || loading || mountedLoading) ?
        <Skeleton className='h-[35px] w-[170px] rounded-full' />:
        (
          (activeCurriculum && btnText && currentProgress) &&
          <Button type={btnColor} disabled={loadingReviewCurriculum || openProgressLoading} className='min-h-[35px]' onClick={() => {
            if (!loadingReviewCurriculum && !openProgressLoading) {
              _handleNextBtn(btnType)
            }
          }}>
              {
                loadingReviewCurriculum ? <Loader />: 
                (
                  openProgressLoading || loadingPickTask ?
                  <LoaderPulse color="loader-dark" />:
                  btnText
                )
              }
          </Button>
        )
      }
      {
          !dismissReviewModal &&
          createPortal(
              <ShowReviewModal curriculum={activeCurriculum} modalDismissed={dismissReviewModal} setModalDismissed={setDismissedReviewModal} onConfirm={() => {}} />,
              document.body
          )
      }
      {
        openSectionModal &&
        createPortal(
          <Modal isDismissed={!openSectionModal} setIsDismissed={() => setOpenSectionModal(!openSectionModal)} onConfirm={() => _handleOpenNewSection()} title="Open Section" 
          confirmBtnText={"Open Section"}
          buttonLoading={loadingReviewCurriculum || openProgressLoading}
          />,
          document.body
        )
      }
    </div>
  )
}

export default memo(NextBtnCurriculum)