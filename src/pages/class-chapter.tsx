import { Outlet, useNavigate, useParams } from "react-router-dom";
import StudentTemplate from "../components/templates/student-template";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../store";
import { useMemo, useEffect } from 'react'
import { classActions, classCategoryById, classCurriculumById, selectClassUserReviewCurriculum, setActiveClassState } from "../store/classSlice";
import { fetchCurriculumApi } from "../api/classApi";
import { canAccessCurriculum, getRegistrantByClassId, getRegistrantLastProgress, getRegistrantProgressAll, registrantActions, registrantLoading, selectRegistrantProgressByClassId, selectRegistrantProgressByCurriculumId } from "../store/registrantSlice";
import { fetchExistingReviewApi, fetchRegistrantProgressApi, openCurrentProgressApi, startClassApi } from "../api/registrantApi";
import Loader, { LoaderPulse } from "../components/all/loader";
import { createPortal } from "react-dom";
import Modal from "../components/molecules/modal";
import { toast } from "react-toastify";
import { setActiveCollapse } from "../store/collapseSlice";
import { bannerActions } from "../store/bannerSlice";

export default function ClassChapter() {
    const { classId,curriculumId } = useParams()
    const web = useNavigate()
    const dispatch: AppDispatch = useDispatch()
    const currentClassProgress = useSelector((state:RootState) => selectRegistrantProgressByClassId(state,classId as unknown as number))
    const registrant = useSelector((state: RootState) => getRegistrantByClassId(state, classId as unknown as number))
    const registrantId = registrant?.id
    const lastProgress = useSelector(getRegistrantLastProgress)
    const currentProgress = useSelector((state:RootState) => selectRegistrantProgressByCurriculumId(state,curriculumId as unknown as number))
    const curriculumFromParams = useSelector((state:RootState) => classCurriculumById(state,curriculumId as unknown as number))
    const progressCurriculum = useSelector((state: RootState) => classCurriculumById(state,lastProgress?.curriculum_id as unknown as number))
    const progressCategoryId = progressCurriculum?.curriculum_category_id
    const progressCategory = useSelector((state:RootState) => classCategoryById(state,progressCategoryId as unknown as number))
    const currentCategory = useSelector((state:RootState) => classCategoryById(state,curriculumFromParams?.curriculum_category_id as unknown as number))
    const currentCategoryAccessible = useSelector((state:RootState) => canAccessCurriculum(state, {
        curriculumCategoryId: curriculumFromParams?.curriculum_category_id as unknown as number,
        registrantId: registrantId as unknown as number
    }))
    const activeCollapse = useSelector((state: RootState) => state.collapseState.activeCollapse)
    const existingReview = useSelector((state: RootState) => selectClassUserReviewCurriculum(state, {
      curriculumId: curriculumId as unknown as number,
      studentId: registrant?.student_id as unknown as number
    }))

    const loading = useSelector(registrantLoading).includes("fetch_progress")
    const startClassLoading = useSelector(registrantLoading).includes("start_class")

    const _openProgress = () => {
      dispatch(openCurrentProgressApi({
        payload: {
          registrant_id: registrant?.id as unknown as number,
          curriculum_id: curriculumId as unknown as number
        }
      })).then((data) => {        
        toast("Chapter opened.", {
          type: 'success'
        })
      }).catch(err => {
        toast("Failed to open chapter.", {
          type: 'error'
        })
      })
    }

    useEffect(() => {
        if (registrantId) {
            dispatch(fetchRegistrantProgressApi({
                payload: {
                    registrant_id: registrantId
                }
            })).then(resp => {
                    
            }).catch(err => {
              toast("Failed to fetch progress.", {
                type: 'error'
              })
            })
        }
    }, [registrantId])

    useEffect(() => {
      if (lastProgress) {
        web(`/my-class/${classId}/material/${lastProgress.curriculum_id}/task`)
      }
    }, [lastProgress?.curriculum_id])

    useEffect(() => {
      if (!Number.isNaN(parseInt(curriculumId ?? ''))) {
        if (currentCategoryAccessible) {
          dispatch(classActions.setActiveCurriculum(curriculumId)) 
          if (activeCollapse != curriculumFromParams?.curriculum_category_id) {
            dispatch(setActiveCollapse(curriculumFromParams?.curriculum_category_id))            
          }
          if (
            ((progressCategory?.id == currentCategory?.id && (curriculumFromParams?.sort as unknown as number) < (progressCurriculum?.sort as unknown as number)) || 
            ((currentCategory?.sort as unknown as number) < (progressCategory?.sort as unknown as number))) &&
            !currentProgress
          ) {
            _openProgress()
          }        
          if (!existingReview) {
            dispatch(fetchExistingReviewApi({
              payload: {
                curriculum_id: curriculumId as unknown as number,
                registrant_id: registrant?.id as unknown as number
              }
            }))
          }   
        } else {
          if (currentClassProgress.length > 0 && !loading && loading !== undefined) {

          }                
        }
      }
    }, [curriculumId])

    useEffect(() => {
        return () => {
            dispatch(bannerActions.removeBanner("lock_curriculum"))
            dispatch(classActions.setActiveClassState(undefined))
            dispatch(classActions.setActiveCurriculum(undefined))
            dispatch(setActiveCollapse(undefined))
            dispatch(registrantActions.setProgress([]))
        }
    }, [])

    return (
        <StudentTemplate.ClassChapter>
            {
                loading && 
                <div className="w-full h-full flex items-center justify-center mt-6"><LoaderPulse color="loader-light" /></div>
            }
            {
                (currentClassProgress.length > 0 && !loading) &&
                <Outlet />
            }
            {
                (!loading && currentClassProgress.length == 0) &&
                createPortal(
                    <Modal isDismissed={false} 
                    buttonLoading={startClassLoading}
                    confirmBtnText={'Mulai Kelas'}
                    onConfirm={() => {
                        dispatch(startClassApi({
                            payload: {
                                registrant_id: registrant?.id as unknown as number
                            }
                        })).then(resp => {
                            
                        }).catch((err) => {
                            toast(err.message,{
                                type: "error"
                            })
                        })
                    }}
                    setIsDismissed={() => {
                        web(-1)
                    }} type="info" title="Informasi" body={
                        <div>Anda belum memulai kelas. <span className="underline">Klik untuk memulai kelas</span></div>
                    } />,
                    document.body
                )
            }
        </StudentTemplate.ClassChapter>
    )
}
