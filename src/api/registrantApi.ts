import { createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios"
import { registrantActions } from "../store/registrantSlice"
import { classActions } from "../store/classSlice"
import { AppDispatch } from "../store"

type FetchRegistrantProgressApiPayload = {
  payload: {
    registrant_id: number
  }
}
export const fetchRegistrantProgressApi = (_:FetchRegistrantProgressApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async(resolve,reject) => {
    try {
      dispatch(registrantActions.addLoad("fetch_progress"))
      const resp = await axios.get("/class/progress_using_registrant", {
        params: _.payload
      })
      const data = await resp.data
      dispatch(registrantActions.setProgress(data.data.progress))
      dispatch(registrantActions.setTask(data.data.tasks))
      dispatch(registrantActions.removeLoad("fetch_progress"))
      resolve(data)
    } catch (error) {
      dispatch(registrantActions.removeLoad("fetch_progress"))
      reject(error)
    }
  })
}

type SubmitTaskApiPayload = {
  payload: {
    task_id: number,
    answer: string,
    registrant_id: number
  }
}

export const submitTaskApi = createAsyncThunk("services/submit_task", async(_: SubmitTaskApiPayload, {dispatch}) => {
  try {
    dispatch(registrantActions.addLoad("submit_task"))
    const resp = await axios.post("/class/submit_task", _.payload)
    const data = await resp.data
    dispatch(registrantActions.updateTask(data.data))
    dispatch(registrantActions.updateRegistrant(data.data.registrant))
    dispatch(registrantActions.removeLoad("submit_task"))
    return data
  } catch (error) {
    dispatch(registrantActions.removeLoad("submit_task"))
    return error
  }
})

type SubmitReviewApiPayload = {
  payload: {
    registrant_id: number
    curriculum_id: number
    rating: number
    saran: string
  }
}
export const submitReviewApi = (_:SubmitReviewApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async(resolve,reject) => {
    try {
      dispatch(registrantActions.addLoad("submit_review"))
      const resp = await axios.post("/class/submit_review_curriculum", _.payload)
      const data = await resp.data
      dispatch(classActions.appendReviewCurriculum(data.data))
      dispatch(registrantActions.removeLoad("submit_review"))
      resolve(data)
    } catch (error) {
      dispatch(registrantActions.removeLoad("submit_review"))
      reject(error)
    }
  })
}

type ExistingReviewApiPayload = {
  payload: {
    curriculum_id: number
    registrant_id: number
  }
}
export const fetchExistingReviewApi = createAsyncThunk("services/submit_review", async(_: ExistingReviewApiPayload, {dispatch}) => {
  try {
    dispatch(registrantActions.addLoad("fetch_existing_review"))
    const resp = await axios.get("/class/existing_registrant_review", {
      params: _.payload
    })
    const data = await resp.data
    if (data.data) {
      dispatch(classActions.appendReviewCurriculum(data.data))
    }
    dispatch(registrantActions.removeLoad("fetch_existing_review"))
    return data
  } catch (error) {
    dispatch(registrantActions.removeLoad("fetch_existing_review"))
    return error
  }
})

type StartClassApiPayload = {
  payload: {
    registrant_id: number
  }
}
export const startClassApi = (_: StartClassApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async(resolve,reject) => {
    try {
      dispatch(registrantActions.addLoad("start_class"))
      const resp = await axios.post("/class/registrant_start", _.payload)
      const data = await resp.data
      dispatch(registrantActions.updateRegistrant(data.data.registrant))
      dispatch(registrantActions.setProgress(data.data.progress))
      dispatch(registrantActions.removeLoad("start_class"))
      resolve(data)
    } catch (error) {
      dispatch(registrantActions.removeLoad("start_class"))
      reject(error)
    }
  })
}

type OpenNextProgressApiPayload = {
  payload: {
    registrant_id: number,
  }
}
export const openNextProgressApi = (_: OpenNextProgressApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async (resolve,reject) => {
    try {
      dispatch(registrantActions.addLoad("open_next_progress"))
      const resp = await axios.post("class/open_next_progress", _.payload)
      const data = await resp.data
      if (data.data.new_progress) {
        dispatch(registrantActions.appendProgress(data.data.new_progress))
      } else {
        reject({message: "Cant get new progress."})
      }
      dispatch(registrantActions.removeLoad("open_next_progress"))
      resolve(data)
    } catch (error) {
      dispatch(registrantActions.removeLoad("open_next_progress"))
      reject(error)
    }
  })
}
type OpenNextCurrentApiPayload = {
  payload: {
    registrant_id: number,
    curriculum_id: number
  }
}
export const openCurrentProgressApi = (_: OpenNextCurrentApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async (resolve,reject) => {
    try {
      dispatch(registrantActions.addLoad("open_next_progress"))
      const resp = await axios.post("class/open_current_progress", _.payload)
      const data = await resp.data
      if (data.data.new_progress && data.data.registrant) {
        dispatch(registrantActions.appendProgress(data.data.new_progress))
        dispatch(registrantActions.updateRegistrant(data.data.registrant))
      } else {
        reject({message: "Cant get new progress."})
      }
      dispatch(registrantActions.removeLoad("open_next_progress"))
      resolve(data)
    } catch (error) {
      dispatch(registrantActions.removeLoad("open_next_progress"))
      reject(error)
    }
  })
}

type PickTaskApiPayload = {
  payload: {
    registrant_id: number,
    curriculum_id: number
  }
}
export const pickTaskApi = (_: PickTaskApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async (resolve,reject) => {
    try {
      dispatch(registrantActions.addLoad("pick_task"))
      const resp = await axios.post("class/pick_task", _.payload)
      const data = await resp.data
      if (data.data.tasks) {
        dispatch(registrantActions.appendTasks(data.data.tasks))
      } else {
        reject({message: "Cant get tasks."})
      }
      if (data.data.tasks) {
        dispatch(registrantActions.updateRegistrant(data.data.registrant))
      }
      dispatch(registrantActions.removeLoad("pick_task"))
      resolve(data)
    } catch (error) {
      dispatch(registrantActions.removeLoad("pick_task"))
      reject(error)
    }
  })
}