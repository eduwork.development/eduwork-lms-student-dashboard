import { createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios"
import { app } from "../config/app"
import { initActions } from "../store/initSlice"
import { authActions } from "../store/authSlice"
import { AxiosWrapper } from "../utils/base-axios"
import { AppDispatch } from "../store"


type LoginApiPayload = {
  payload: {}
}

export const LoginApi = (_: LoginApiPayload) => (dispatch: AppDispatch) => {
  return new Promise(async(resolve, reject) => {
    try {
      const resp = await axios.post(`${app.APP_URL}/ap/lmsstudentapi/get_auth`, {}, {
        withCredentials: true
      })
      const data = await resp.data
      AxiosWrapper.init({
        authorization: data.api_token
      })
      dispatch(authActions.storeUser(data))
      dispatch(initActions.removeLoad("auth"))
      resolve(data)
    } catch (error) {
      reject(error)
    }
  })
}

export const login = async(_: {payload: {email: string, password: string}}) => {
  return new Promise(async (resolve,reject) => {
    try {
      const resp = await axios.post(`${app.APP_URL}/login?permalink=${window.location.href}`, _.payload, {
        method: "POST",
        headers: {
          "X-Requested-With": "XMLHttpRequest",
          "Content-Type": "application/json"
        }
      })
      const data = await resp.data
      return resolve(data)
    } catch (error) {
      return reject(error)
    }
  })
}