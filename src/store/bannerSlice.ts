import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { BannerStateInterface, BannerType } from "./types/BannerTypes";
import BaseReducer from "../utils/base-reducer";
import { RootState } from ".";

const initialState: BannerStateInterface = {
  open: [],
  data: '',
  loads: []
}

const BannerSlice = createSlice({
  name: "services/banner",
  initialState,
  reducers: {
    ...BaseReducer,

    addBanner: (state, action: PayloadAction<BannerType>) => {
      const payload = action.payload; // Assuming payload is a string

      // Check if the payload already exists in the 'open' array
      if (!state.open.some(banner => banner === payload)) {
        // If it doesn't exist, add it to the 'open' array
        state.open.push(payload);
      }

    },

    addBannerData: (state,action: PayloadAction<string>) => {
      state.data = action.payload
    },

    removeBanner: (state, action: PayloadAction<BannerType>) => {
      const payload = action.payload; // Assuming payload is a string

      // Find the index of the payload in the 'open' array
      const index = state.open.findIndex(banner => banner === payload);

      if (index !== -1) {
        // If it exists, remove it from the 'open' array
        state.open.splice(index, 1);
      }

      state.data = ''
    }
  }
});

export const bannerActions = BannerSlice.actions

export const selectBanner = {
  opens: (state: RootState) => state.bannerState.open,
  loads: (state: RootState) => state.bannerState.loads,
  data: (state:RootState) => state.bannerState.data
}

export default BannerSlice.reducer