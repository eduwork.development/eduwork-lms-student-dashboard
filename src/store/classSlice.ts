import { createSelector, createSlice } from "@reduxjs/toolkit";
import { RootState } from ".";
import { ClassStateInterface } from "./types/ClassTypes";
import BaseReducer from "../utils/base-reducer";

const initialState: ClassStateInterface = {
    class: [],
    curriculum: [],
    category: [],
    otherMaterial: [],
    reviewCurriculum: [],
    task: [],
    loads: [],
    activeClass: {},
    activeNav: 'material',
    activeCurriculum: null
}

export const classSlice = createSlice({
    name: 'classState',
    initialState,
    reducers: {
        ...BaseReducer,
        setClassState: (state, action) => {
            state.class = action.payload
        },
        setActiveClassState: (state, action) => {
            state.activeClass = action.payload
        },
        setActiveCurriculum: (state, {payload}) => {
            state.activeCurriculum = payload
        },
        setCategories: (state, {payload}) => {
            state.category = payload
        },
        setCurriculums: (state, {payload}) => {
            state.curriculum = payload
        },
        setOtherMaterials: (state, {payload}) => {
            state.otherMaterial = payload
        },
        
        setReviewCurriculums: (state, {payload}) => {
            state.reviewCurriculum = payload
        },
        appendReviewCurriculum: (state, {payload}) => {
            const index = state.reviewCurriculum.findIndex(item => item.id == payload.id)
            if (index == -1) {
                state.reviewCurriculum.splice(index, 0, payload)
            } 
        },
        setActiveNav: (state, {payload}) => {
          state.activeNav = payload
        },
        setTasks: (state, {payload}) => {
            state.task = payload
        }
    }
})

export const { setClassState, setActiveClassState } = classSlice.actions
export const classActions = classSlice.actions

export const classActiveCurriculum = (state:RootState) => state.classState.activeCurriculum

export const classLoading = (state:RootState) => state.classState.loads

// CLASS
export const classAll = (state:RootState) => state.classState.class
export const classById = createSelector([classAll, (state:RootState, class_id: number) => class_id], (classes, class_id) => {
    return classes.find(item => item.id == class_id)
})

// CURRICULUM CATEGORY 
export const classCategoryAll = (state:RootState) => state.classState.category
export const classCategoryByClassId = createSelector([classCategoryAll, (state:RootState, class_id: number) => class_id], (categories, class_id) => {
    return categories.filter(item => item.class_id == class_id).sort((a, b) => (a.sort || 0) - (b.sort || 0))
})
export const classCategoryById = createSelector([classCategoryAll, (state:RootState, id: number) => id], (categories, id) => {
    return categories.find(item => item.id == id)
})

// CURRICULUM
export const classCurriculumAll = (state:RootState) => state.classState.curriculum
export const classCurriculumByCategoryId = createSelector([classCurriculumAll, (state:RootState, category_id: number) => category_id], (curriculums, category_id) => {
    return curriculums.filter(item => item.curriculum_category_id == category_id).sort((a, b) => (a.sort || 0) - (b.sort || 0))
})
export const classCurriculumByCategoryIds = createSelector([classCurriculumAll, (state:RootState, categoryIds: Array<number>) => categoryIds], (curriculums, categoryIds) => {
  return curriculums.filter(item => categoryIds.includes(item.curriculum_category_id as unknown as number))
})
export const classCurriculumById = createSelector([classCurriculumAll, (state:RootState, id: number) => id], (curriculums, id) => {
    return curriculums.find(item => item.id == id)
})

// OTHER MATERIAL
export const classOtherMaterialAll = (state:RootState) => state.classState.otherMaterial

// REVIEW CURRICULUM
export const classReviewCurriculumAll = (state:RootState) => state.classState.reviewCurriculum
export const selectClassReviewCurriculumByCurriculumId = createSelector([classReviewCurriculumAll, (state: RootState, curriculumId: number) => curriculumId], (reviews,curriculumId) => {
    return reviews.filter(item => item.curriculum_id == curriculumId)
})
export const selectClassUserReviewCurriculum = createSelector([classReviewCurriculumAll,(state: RootState, args: {
    curriculumId: number
    studentId: number
}) => ({
    ...args    
})], (reviews,data) => {
    return reviews.find(item => item.curriculum_id == data.curriculumId && item.student_id == data.studentId)
})

// TASK
export const classTaskAll = (state:RootState) => state.classState.task
export const classTaskByCurriculumId = createSelector([classTaskAll, (state: RootState, curriculum_id) => curriculum_id], (tasks, curriculum_id) => {
    return tasks.filter(item => item.curriculum_id == curriculum_id)
})

export const isPenyaluranClass = createSelector([(state: RootState, classId) => classById(state,classId)], (classes) => {
  const title = (
    classes?.title === 'Penyaluran Programer' ||
    classes?.title === 'Penyaluran Quality Assurance' ||
    classes?.title === 'Penyaluran Digital marketing' ||
    classes?.id === 69 ||
    classes?.id === 148
  );

  return title;
})
export const isFreeClass = createSelector([(state: RootState, classId) => classById(state,classId)], (classes) => {
  const isaPrice = (classes?.isa_price ?? 0) <= 0;
  const regularPrice = (classes?.regular_price ?? 0) <= 0;
  const title = (
    classes?.title !== 'Penyaluran Programer' &&
    classes?.title !== 'Penyaluran Quality Assurance' &&
    classes?.title !== 'Penyaluran Digital marketing' &&
    classes?.id !== 69 &&
    classes?.id !== 148
  );

  return isaPrice && regularPrice && title;
})

export const selectNextCurriculum = createSelector([(state: RootState, classId: number) => {
  const categories = classCategoryByClassId(state,classId)
  const categoryIds = categories.map(item => item.id)
  const curriculums = classCurriculumByCategoryIds(state,categoryIds)
  const activeCurriculumId = classActiveCurriculum(state)
  const activeCurriculum = classCurriculumById(state,activeCurriculumId as unknown as number)
  return {
    activeCurriculum,
    categories,
    curriculums
  }
}], ({categories, activeCurriculum, curriculums}) => {
  let nextCurriculum = null
  const sortedCategories = categories.sort((a, b) => a.sort - b.sort);

  const activeCategory = sortedCategories.find(category => category.id === activeCurriculum?.curriculum_category_id);

  if (activeCategory) {
    const activeCategoryIndex = sortedCategories.indexOf(activeCategory);

    if (activeCategoryIndex < sortedCategories.length - 1) {
      const nextCategory = sortedCategories[activeCategoryIndex + 1];

      const nextCategoryCurriculums = curriculums
        .filter(curriculum => curriculum.curriculum_category_id === nextCategory.id)
        .sort((a, b) => a.sort - b.sort);

      if (nextCategoryCurriculums.length > 0) {
        nextCurriculum = nextCategoryCurriculums.first();
      }
    }
  }

  return nextCurriculum
})
export const selectNextCurriculumInCategory = createSelector(
  [(state: RootState, { categoryId, activeCurriculumId }: { categoryId: number, activeCurriculumId: number }) => {
    return {
      curriculums: classCurriculumByCategoryId(state, categoryId),
      activeCurriculum: classCurriculumById(state, activeCurriculumId)
    };
  }],
  ({ curriculums, activeCurriculum }) => {
    // Sort the curriculums by 'sort' property
    curriculums.sort((a, b) => a.sort - b.sort);

    if (activeCurriculum) {
      // Find the index of the active curriculum
      const activeIndex = curriculums.findIndex(curriculum => curriculum.id === activeCurriculum.id);

      if (activeIndex !== -1 && activeIndex < curriculums.length - 1) {
        // Get the next curriculum after the active one
        const nextCurriculum = curriculums[activeIndex + 1];
        return nextCurriculum;
      }
    }

    // If there's no active curriculum or it's the last one, return null
    return null;
  }
);


export default classSlice.reducer