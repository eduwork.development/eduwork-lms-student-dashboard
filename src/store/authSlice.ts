import { createSlice } from "@reduxjs/toolkit"
import BaseReducer from "../utils/base-reducer"
import { RootState } from "."
import { AuthStateInterface } from "./types/AuthTypes"
import { AxiosWrapper } from "../utils/base-axios"

const initialState: AuthStateInterface = {
  user: null,
  loads: []
}

const AuthSlice = createSlice({
  name: 'slice/auth',
  initialState,
  reducers: {
    ...BaseReducer,
    storeUser: (state, {payload}) => {
      state.user = payload
    },
    login: (state, action) => {
      const token = action.payload.api_token
      localStorage.setItem('api_token', token)
      AxiosWrapper.init({
        authorization: token
      }) 
      state.user = action.payload.user
    }
  }
})

export const authActions = AuthSlice.actions

export const getAuthLoading = (state: RootState) => state.authState.loads
export const getAuthUser = (state: RootState) => state.authState.user

export default AuthSlice.reducer