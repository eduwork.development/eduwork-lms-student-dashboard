export type BannerType = "" | "lock_curriculum"

export interface BannerStateInterface {
  open: Array<BannerType>
  data: string
  loads: Array<string>
}