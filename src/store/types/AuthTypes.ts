import { UserType } from "./UserTypes";

export type AuthServiceMessageType = {
  text: string;
  type: 'SUCCESS' | 'INFO' | 'ERROR' | null;
}

export interface AuthStateInterface {
  user: UserType | null,
  loads: Array<string>
}