import { RegistrantType, TaskType } from '@/src/store/types/RegistrantTypes';
import { PayloadAction, createSelector, createSlice } from "@reduxjs/toolkit"
import BaseReducer from "../utils/base-reducer"
import { RootState } from "."
import { RegistrantInterface } from "./types/RegistrantTypes"
import moment from "moment"
import { classById, classCategoryAll, classCategoryById, classCurriculumByCategoryId, classCurriculumById, classTaskAll, isFreeClass, isPenyaluranClass } from './classSlice';
import { CurriculumType } from './types/ClassTypes';
import { StatusType } from './hooks/class-hooks';

const initialState: RegistrantInterface = {
  registrant: [],
  progress: [],
  task: [],
  loads: []
}

const RegistrantSlice = createSlice({
  name: 'services/registrant',
  initialState,
  reducers: {
    ...BaseReducer,
    setRegistrants: (state, {payload}) => {
      state.registrant = payload
    },
    setProgress: (state, {payload}) => {
      state.progress = payload
    },
    setTask: (state, {payload}) => {
      state.task = payload
    },
    updateTask: (state, action: PayloadAction<TaskType>) => {
      const index = state.task.findIndex(item => item.id == action.payload.id)
      if (index > -1) {
        state.task.splice(index, 1, action.payload)
      }
    },
    updateRegistrant: (state, action: PayloadAction<RegistrantType>) => {
      const index = state.registrant.findIndex(item => item.id == action.payload.id)
      if (index > -1) {
        state.registrant.splice(index, 1, action.payload)
      }
    },
    appendProgress: (state, {payload}) => {
      const progressIndex = state.progress.findIndex(item => item.id == payload.id)
      if (progressIndex === -1) {
        state.progress.splice(0,0,payload)        
      } else {
        state.progress.splice(progressIndex,1,payload)
      }
    },
    appendTasks: (state, {payload}) => {
      state.task = [...state.task, ...payload]
    }
  }
})

export const registrantActions = RegistrantSlice.actions

export const registrantLoading = (state:RootState) => state.registrantState.loads

// REGISTRANT
export const getRegistrantAll = (state:RootState) => state.registrantState.registrant
export const getRegistrantByClassId = createSelector([getRegistrantAll, (state: RootState, class_id: number) => class_id], (registrants, class_id) => {
  return registrants.find(item => item.class_id == class_id)
})
export const getRegistrantById = createSelector([getRegistrantAll, (state: RootState, registrantId: number) => registrantId], (registrants, registrantId) => {
  return registrants.find(item => item.id == registrantId)
})

// PROGRESS
export const getRegistrantProgressAll = (state:RootState) => state.registrantState.progress
export const getRegistrantLastProgress = (state: RootState) => {
  const progress = [...state.registrantState.progress]; // Create a shallow copy of the progress array
  progress.sort((a, b) => {
    const dateA = moment(b.created_at);
    const dateB = moment(a.created_at);

    if (dateA.isBefore(dateB)) {
      return -1;
    } else if (dateA.isAfter(dateB)) {
      return 1;
    } else {
      // If created_at dates are the same, sort by id
      return b.id - a.id;
    }
  });
  return progress[0];
};
export const selectRegistrantProgressByCurriculumId = createSelector([getRegistrantProgressAll, (state: RootState, curriculum_id: number) => curriculum_id], (progress, curriculum_id) => {
  return progress.find(item => item.curriculum_id == curriculum_id)
})
export const selectRegistrantProgressByClassId = createSelector([getRegistrantProgressAll, (state: RootState, classid: number) => classid], (progress, classid) => {
  return progress.filter(item => item.class_id == classid).sort((a, b) => {
    const dateA = moment(b.created_at);
    const dateB = moment(a.created_at);

    return dateA.isBefore(dateB) ? -1 : dateA.isAfter(dateB) ? 1 : 0;
  });
})
export const selectRegistrantProgressAllByCategoryId = createSelector(
  [getRegistrantProgressAll, (state: RootState, categoryId: number) => ({
    category: classCategoryById(state,categoryId),
    curriculums: classCurriculumByCategoryId(state, categoryId)
  })],
  (progress, _) => {
    const curriculumIds = _.curriculums.map(item => item.id)
    return progress.filter(item => curriculumIds.includes(item.curriculum_id as unknown as number))
  }
)

// TASK
export const getRegistrantTaskAll = (state:RootState) => state.registrantState.task
export const getRegistrantTaskUnresolved = createSelector([getRegistrantTaskAll], (tasks) => {
  return tasks.filter(item => item.status == 'progress')
})
export const getRegistrantTaskUnfinished = createSelector([getRegistrantTaskAll], (tasks) => {
  return tasks.filter(item => item.status != 'finish')
})
export const selectRegistrantTaskAllByCurriculumId = createSelector([getRegistrantTaskAll, (state: RootState, curriculum_id) => curriculum_id], (tasks, curriculum_id) => {
  return tasks.filter(item => item.curriculum_id == curriculum_id)
})
export const selectRegistrantTaskUnresolvedByCurriculumId = createSelector([getRegistrantTaskAll, (state: RootState, curriculum_id) => curriculum_id], (tasks, curriculum_id) => {
  return (tasks.filter(item => item.status == 'progress' && item.curriculum_id == curriculum_id).length > 0)
})
export const selectRegistrantTaskResolvedByCurriculumId = createSelector([getRegistrantTaskAll, (state: RootState, curriculum_id) => curriculum_id], (tasks, curriculum_id) => {
  return tasks.filter(item => (item.status == 'done') && item.curriculum_id == curriculum_id).length > 0
})
export const selectRegistrantTaskFinishedByCurriculumId = createSelector([getRegistrantTaskAll, (state: RootState, curriculum_id) => curriculum_id], (tasks, curriculum_id) => {
  return tasks.filter(item => (item.status == 'finish') && item.curriculum_id == curriculum_id).length > 0
})
export const selectRegistrantTaskAllByCategoryId = createSelector(
  [getRegistrantTaskAll, (state: RootState, categoryId: number) => ({
    category: classCategoryById(state,categoryId),
    curriculums: classCurriculumByCategoryId(state, categoryId)
  })],
  (tasks, _) => {
    const curriculumIds = _.curriculums.map(item => item.id)
    return tasks.filter(item => curriculumIds.includes(item.curriculum_id as unknown as number))
  }
)

// HELPER
type SelectRegistrantCurriculumStatusArgs = {
  curriculumId: number
  activeCurriculum: CurriculumType | undefined
}
export const selectRegistrantCurriculumStatus = createSelector([(state: RootState, args: SelectRegistrantCurriculumStatusArgs) => ({
  classTasks: classTaskAll(state),
  tasks: selectRegistrantTaskAllByCurriculumId(state,args.curriculumId),
  progress: selectRegistrantProgressByCurriculumId(state,args.curriculumId),
  curriculum: classCurriculumById(state,args.curriculumId),
  categories: classCategoryAll(state),
  activeCategory: classCategoryById(state, args.activeCurriculum?.curriculum_category_id as unknown as number),
  activeCurriculum: args.activeCurriculum
})], (dataRow) => {
  const currentCurriculumCategory = dataRow.categories.find(item => item.id == dataRow.curriculum?.curriculum_category_id)
  
  const activeCurriculumCategory = dataRow.activeCategory
  const activeCurriculum = dataRow.activeCurriculum

  const curriculum = dataRow.curriculum
  const currentLocked = ((currentCurriculumCategory?.sort ?? 0) == (activeCurriculumCategory?.sort ?? 0) && (curriculum?.sort as unknown as number) > (activeCurriculum?.sort as unknown as number)) || (currentCurriculumCategory?.sort as unknown as number) > (activeCurriculumCategory?.sort as unknown as number)
  let currentStatusLocal: StatusType = null
  // is locked
  if (currentLocked) {
    currentStatusLocal = 'locked'
  } else {
    const currentProgress = dataRow.progress
    const currentClassTaskAll = dataRow.classTasks.filter(item => item.curriculum_id == curriculum?.id as unknown as number)
    const currentTaskFinished = dataRow.tasks.filter(item => (item.status == 'finish') && item.curriculum_id == curriculum?.id as unknown as number).length > 0
    const currentTaskResolved = dataRow.tasks.filter(item => (item.status == 'done') && item.curriculum_id == curriculum?.id as unknown as number).length > 0
    const currentTaskUnresolved = dataRow.tasks.filter(item => item.status == 'progress' && item.curriculum_id == curriculum?.id as unknown as number).length > 0
    // is unlocked
    // unlock but not opened
    if (!currentProgress) {
      currentStatusLocal = 'unlocked'
    }
    // finished 
    else if (currentProgress.status == 'finish' || (currentClassTaskAll.length > 0 && currentTaskFinished) || currentClassTaskAll.length == 0) {
      currentStatusLocal = 'finished'
    }
    // waiting
    else if(currentProgress.status == 'done' && (currentClassTaskAll.length > 0 && currentTaskResolved)) {
      currentStatusLocal = 'waiting'
    }
    // unresolve
    else if(currentTaskUnresolved && currentClassTaskAll.length > 0) {
      currentStatusLocal = 'unfinished'
    }
  }

  return currentStatusLocal
})
type SelectRegistrantCategoryStatusArgs = {
  categoryId: number
  activeCurriculum: CurriculumType | undefined
}
export const selectRegistrantCategoryStatus = createSelector([(state: RootState, args: SelectRegistrantCategoryStatusArgs) => ({
  classTasks: classTaskAll(state),
  tasks: selectRegistrantTaskAllByCategoryId(state,args.categoryId),
  progress: selectRegistrantProgressAllByCategoryId(state,args.categoryId),
  curriculums: classCurriculumByCategoryId(state,args.categoryId),
  category: classCategoryById(state,args.categoryId),
  activeCategory: classCategoryById(state, args.activeCurriculum?.curriculum_category_id as unknown as number),
  activeCurriculum: args.activeCurriculum
})], (dataRow) => {
  const statusses: Array<StatusType> = []
  const currentCurriculumCategory = dataRow.category
  
  const activeCurriculumCategory = dataRow.activeCategory
  const activeCurriculum = dataRow.activeCurriculum

  dataRow.curriculums.map(curriculum => {
    const currentLocked = ((currentCurriculumCategory?.sort ?? 0) == (activeCurriculumCategory?.sort ?? 0) && ((curriculum.sort as unknown as number) > (activeCurriculum?.sort as unknown as number))) || (currentCurriculumCategory?.sort as unknown as number) > (activeCurriculumCategory?.sort as unknown as number)
    let currentStatusLocal: StatusType = null
    // is locked
    if (currentLocked) {
      currentStatusLocal = 'locked'
    } else {
      const currentProgress = dataRow.progress.find(item => item.curriculum_id == curriculum.id)
      const currentClassTaskAll = dataRow.classTasks.filter(item => item.curriculum_id == curriculum.id)
      const currentTaskFinished = dataRow.tasks.filter(item => (item.status == 'finish') && item.curriculum_id == curriculum.id).length > 0
      const currentTaskResolved = dataRow.tasks.filter(item => (item.status == 'done') && item.curriculum_id == curriculum.id).length > 0
      const currentTaskUnresolved = dataRow.tasks.filter(item => item.status == 'progress' && item.curriculum_id == curriculum.id).length > 0
      // is unlocked
      // unlock but not opened
      if (!currentProgress) {
        currentStatusLocal = 'unlocked'
      }
      // finished 
      else if (currentProgress.status == 'finish' || (currentClassTaskAll.length > 0 && currentTaskFinished) || currentClassTaskAll.length == 0) {
        currentStatusLocal = 'finished'
      }
      // waiting
      else if(currentProgress.status == 'done' && (currentClassTaskAll.length > 0 && currentTaskResolved)) {
        currentStatusLocal = 'waiting'
      }
      // unresolve
      else if(currentTaskUnresolved && currentClassTaskAll.length > 0) {
        currentStatusLocal = 'unfinished'
      }
    }
    statusses.push(currentStatusLocal)
  })

  return statusses
})

type CanAccessCurriculumType = {
  curriculumCategoryId: number
  registrantId: number
}
export const canAccessCurriculum = createSelector([(state: RootState, {
  curriculumCategoryId, registrantId
}: CanAccessCurriculumType) => {
  const registrant = getRegistrantById(state,registrantId)
  const curriculumCategory = classCategoryById(state, curriculumCategoryId)
  const classes = classById(state,registrant?.class_id as unknown as number)
  const isPenyaluran = isPenyaluranClass(state, classes?.id as unknown as number)
  const isFree = isFreeClass(state,classes?.id)
  const freeOpen = !['private','bootcamp'].includes(classes?.type ?? '')
  return {
    curriculumCategory,
    registrant,
    isPenyaluran,
    isFree,
    freeOpen
  }
}], ({curriculumCategory,registrant,isPenyaluran,isFree,freeOpen}) => {
  if (registrant) {
    // FREE OPEN
    if (freeOpen) {
      return true
    }
    // PENYALURAN CLASS
    if (isPenyaluran) {
      return true;
    }
    // PUBLIC
    if (curriculumCategory?.accessible === 'public') {
      return true;
    }
    // FREE CLASS
    if (isFree) {
      if (!['unprocessed', 'confirm', 'interview',''].includes(registrant?.status ?? '')) {
        return true;
      }
    }
    // TRIAL
    if ((['incubation','document', 'pool'].includes(registrant?.status ?? '')) && curriculumCategory?.accessible == 'trial') {
      return true
    }
    // APPROVED
    if (['done', 'approved', 'graduation', 'working'].includes(registrant.status ?? '')) {
      return true
    }
  }

  return false;
})


export default RegistrantSlice.reducer