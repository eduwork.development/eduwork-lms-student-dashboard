import '@/src/loader.css'

export default function Loader({className}:{className?: string}) {
  return (
    <div
    className={`inline-block h-5 w-5 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite] ${className}`}
    role="status">
        <span
            className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]"
            ></span>
    </div>
  )
}

type LoaderPulseProps = {
  color?: "loader-light" | "loader-dark";
}

export function LoaderPulse({color}: LoaderPulseProps) {
  return (
    <span
      className="text-white font-bold rounded-full flex items-center justify-center relative space-x-1"
    >
      {/* <span className={`loader ${color ?? ''} animate-loader`}></span>
      <span className={`loader ${color ?? ''} animate-loader animation-delay-200`}></span>
      <span className={`loader ${color ?? ''} animate-loader animation-delay-400`}></span> */}
      <div className={`dot1 ${color ?? ''}`}></div>
      <div className={`dot2 ${color ?? ''}`}></div>
      <div className={`dot3 ${color ?? ''}`}></div>
    </span> 
  )
}