import { ReactNode } from "react";

interface DropdownProps {
    className?: string;
    children: string | ReactNode
    onChange?: Function
    defaultValue?: string
}

export default function Dropdown({ className, children, onChange, defaultValue }: DropdownProps) {
    return (
        <select onChange={(e) => onChange ? onChange(e): ''} value={defaultValue} className={`rounded-full border border-primary-main text-primary-main px-4 py-1.5 ${className}`}>
            {children}
        </select>
    )
}
