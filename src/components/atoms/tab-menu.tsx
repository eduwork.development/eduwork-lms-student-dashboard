import { NavLink } from "react-router-dom";

interface TabMenuProps {
    path: string;
    text: string;
    isActive?: boolean;
}

export default function TabMenu({ path, text, isActive }: TabMenuProps) {
    return (
        <NavLink
            to={path}
            className={`border-4 border-transparent flex items-center justify-center px-2 h-full ${isActive ? 'active-nav-list' : 'text-neutral-70'} hover:active-nav-list`}
        >
            {text}
        </NavLink>
    )
}
