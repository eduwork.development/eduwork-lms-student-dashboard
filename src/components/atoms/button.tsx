import { ReactNode, MouseEventHandler } from 'react'

export type ButtonColorType = 'primary' | 'success' | 'danger' | 'warning' | 'light' | 'info'
interface ButtonProps {
    text?: string | ReactNode;
    type?: ButtonColorType;
    borderRadius?: 'lg' | 'full'
    size?: 'small' | 'normal'
    className?: string;
    children?: ReactNode;
    onClick?: MouseEventHandler
    disabled?: boolean
}

export default function Button({ text, type = 'primary', borderRadius = 'full', size = 'normal', className, children, onClick, disabled }: ButtonProps) {
    let _bg = type === 'primary' ? 'bg-primary-main' : type === 'success' ? 'bg-flowkit-green' : type === 'danger' ? 'bg-danger-main' : type === 'warning' ? 'bg-secondary-main' : type === 'light' ? 'bg-white' : type === 'info' ? 'bg-[#17a2b8]': ''
    let _cr = type === 'light' ? 'text-neutral-90' : 'text-white'
    let _size = size === 'small' ? 'px-4 py-2 text-sm' : size === 'normal' ? 'px-5 py-2' : ''

    return (
        <button
            onClick={(e) => {
              if (!disabled && onClick) {
                onClick(e)
              }
            }}
            className={`rounded-${borderRadius} ${_size} whitespace-nowrap shadow ${!disabled ? _bg: 'cursor-default bg-gray-300'} ${_cr} ${className}`}
        >
            {text || children}
        </button>
    )
}
