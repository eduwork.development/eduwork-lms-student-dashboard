import {ReactNode} from 'react'
import {useEffect,useState,SetStateAction,Dispatch,MouseEventHandler} from 'react'
import { Outlet, useParams } from "react-router-dom";
import Button, { ButtonColorType } from "../../atoms/button";
import TabMenu from "../../atoms/tab-menu";
import ClassSection from "../class-section";
import Alert from "../../molecules/alert";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/src/store";
import { classActions, classActiveCurriculum, classById, classCategoryById, classCurriculumById, classLoading, selectClassUserReviewCurriculum } from "@/src/store/classSlice";
import Skeleton from "../../molecules/skeleton";
import { canAccessCurriculum, getRegistrantByClassId, getRegistrantLastProgress, registrantLoading, selectRegistrantProgressByCurriculumId } from "@/src/store/registrantSlice";
import { toast } from 'react-toastify';
import { fetchExistingReviewApi, submitReviewApi } from '@/src/api/registrantApi';
import { CurriculumType } from '@/src/store/types/ClassTypes';
import Loader from '../../all/loader';
import { HiArrowRight, HiOutlineLockClosed, HiOutlineLockOpen, HiStar, HiXMark } from 'react-icons/hi2';
import { createPortal } from 'react-dom';
import { fetchReviewCurriculumApi } from '@/src/api/classApi';
import NextBtnCurriculum from '@/src/pages/classes/next-btn-curriculum';
import { bannerActions } from '@/src/store/bannerSlice';
import { app } from '@/src/config/app';

interface ShowReviewModalProps {
    modalDismissed: boolean;
    setModalDismissed: Dispatch<SetStateAction<boolean>>;
    onConfirm?: Function
    curriculum: CurriculumType | undefined
}
  
export function ShowReviewModal({modalDismissed,setModalDismissed,onConfirm,curriculum}: ShowReviewModalProps) {
  const dispatch: AppDispatch = useDispatch()
  const {classId} = useParams()
  const registrant = useSelector((state: RootState) => getRegistrantByClassId(state,classId as unknown as number))
  const curriculumCategory = useSelector((state: RootState) => classCategoryById(state, curriculum?.curriculum_category_id as unknown as number))
  const submitLoading = useSelector(registrantLoading).includes("submit_review")
  const [answer,setAnswer] = useState('')
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(0);

  const canSubmit = (): boolean => {
      return (!submitLoading && Boolean(answer) && rating > 0)
  }

  const _handleSubmitReview = () => {
      dispatch(submitReviewApi({
          payload: {
            registrant_id: registrant?.id as unknown as number,
            curriculum_id: curriculum?.id as unknown as number,
            saran: answer,
            rating
          }
      })).then((resp: any) => {
          toast("Review Submitted.", {
              type: 'success'
          })
          setModalDismissed(true)
          if (onConfirm) {
            onConfirm()
          }
      }).catch((error: any) => {
          toast("Error Review!", {
              type: 'error'
          })
      })
  }

  return (
      <div className={`fixed inset-0 flex items-center justify-center z-[999] bg-black bg-opacity-50 ${modalDismissed ? 'hidden' : ''}`}>
          <div className="bg-white rounded-lg flex flex-col w-full md:max-w-[450px]">
              <div className="flex flex-col gap-4 p-5">
                  <div className="flex flex-row gap-2 items-start justify-between">
                      <h1 className="text-neutral-90 text-xl font-medium space-y-3">
                          <div className="flex items-center space-x-2">
                              <div className='font-semibold'>Ulas Materi {curriculum?.title}</div>
                          </div>
                          {/* <div className="flex items-center space-x-2">
                              <div className="text-xs p-2 bg-gray-200 rounded-lg">{curriculumCategory?.title}</div>
                              <div>{'>'}</div>
                              <div className="text-xs p-2 bg-gray-200 rounded-lg">{curriculum?.title}</div>
                          </div> */}
                      </h1>
                      <button onClick={() => setModalDismissed(true)} className="bg-gray-100 hover:bg-gray-200 rounded-full p-2"><HiXMark /></button>
                  </div>
              </div>
              <div className="px-5 pb-3"><hr /></div>
              <div className='flex items-center pb-3'>
                  <div className='px-5'>Rating</div>
                  <div className='flex items-center'>
                      {
                          [1,2,3,4,5].map((item) => <button 
                          onClick={() => setRating(item)}
                          onMouseEnter={() => setHover(item)}
                          onMouseLeave={() => setHover(rating)}    
                          key={item} type='button' className={`text-gray-500 hover:text-yellow-500 ${item <= (hover || rating) ? "text-yellow-500" : ""}`}><HiStar className='w-6 h-6' /></button>)
                      }
                  </div>
              </div>
              <div className='px-5 pb-1'>Kritik / Saran</div>
              <div className="px-5 pb-3">
                  <textarea name="" id="" rows={5} className="w-full resize-none border rounded p-4" placeholder="" onChange={(e) => setAnswer(e.target.value)} value={answer}></textarea>
              </div>
              <button className={`px-4 py-3 text-white rounded-b-lg ${!canSubmit() ? 'bg-gray-300': 'bg-blue-600 hover:bg-blue-700'}`} disabled={!canSubmit()} onClick={() => _handleSubmitReview()}>
                  {
                      submitLoading &&
                      <Loader className="mr-2" />
                  }
                  Submit
              </button>
          </div>
      </div>
  )
}

export default function ClassMaterial() {
    // const activeClass: any = useSelector((state: RootState) => state.classState.activeClass)
    const {classId,curriculumId} = useParams()
    const dispatch: AppDispatch = useDispatch()
    const activeClass = useSelector((state:RootState) => classById(state,classId as unknown as number))
    const activeCurriculumId = useSelector(classActiveCurriculum)
    const activeCurriculum = useSelector((state: RootState) => classCurriculumById(state,activeCurriculumId as unknown as number))
    const lastProgress = useSelector(getRegistrantLastProgress)
    const loading = useSelector(classLoading).includes('fetch_curriculum')
    const checkingCurriculum = useSelector(classLoading).includes('checking_active_curriculum')
    const routeCurriculum = useSelector((state:RootState) => classCurriculumById(state,curriculumId as unknown as number))
    const registrant = useSelector((state: RootState) => getRegistrantByClassId(state,classId as unknown as number))
    const categoryAccessible = useSelector((state: RootState) => canAccessCurriculum(state,{
        curriculumCategoryId: routeCurriculum?.curriculum_category_id as unknown as number,
        registrantId: registrant?.id as unknown as number
    }))
    const currentProgress = useSelector((state: RootState) => selectRegistrantProgressByCurriculumId(state, activeCurriculumId as unknown as number))

    useEffect(() => {
        if (!activeCurriculumId && lastProgress) {
            dispatch(classActions.setActiveCurriculum(lastProgress.curriculum_id))
        }
    }, [lastProgress])


    return (
        <>
            <div className="relative w-full bg-red-600 flex items-center">
                <div className="h-[3px] bg-blue-600 z-20 relative" style={{width: `${(registrant?.mentor_progress ?? 0 <= 100 ? registrant?.mentor_progress ?? 0:0)}%`}}></div>
                <div className="h-[3px] bg-red-600 top-0 left-0 z-10" style={{width: `${(registrant?.progress ?? 0) - (registrant?.mentor_progress ?? 0)}%`}}></div>
            </div>
            <div className="flex flex-row items-stretch relative">
                <div className="flex flex-col w-full md:w-3/4 min-h-screen">
                    <div>
                        <div className="flex flex-col sm:flex-row gap-3 md:items-center justify-between bg-primary-surface px-7 py-3 xl:py-0 min-h-[60px] z-10 sticky top-[70px] text-[0.8rem]">
                            {/* breadcrumb */}
                            <div className="flex flex-row gap-2 flex-wrap">
                                <div className="flex flex-row gap-2 items-center">
                                    <span>Kelas {activeClass?.title}</span>
                                    <span>&gt;</span>
                                </div>
                                <div className="flex flex-row gap-2 items-center">
                                    <span className="font-bold">{activeCurriculum?.title ?? '(Locked)'}</span>
                                    {/* {
                                        activeCurriculum &&
                                        <span>&gt;</span>
                                    } */}
                                </div>
                            </div>
                            <NextBtnCurriculum />
                        </div>
                        {/* <Alert type="warning" text="" /> */}
                        {
                            (loading || checkingCurriculum) ?
                            <div className="w-full relative">
                                <Skeleton className="w-full h-[35vw] bg-slate-200" />
                                <div className="absolute top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2 space-x-3 flex items-center justify-center">
                                    <Skeleton className="w-12 h-12 rounded-lg bg-slate-300" />
                                    <Skeleton className="h-7 w-16 rounded-lg bg-slate-300" />
                                </div>
                            </div>:
                            (
                                activeCurriculum && categoryAccessible && currentProgress ?
                                <div className="p-4 static-content" dangerouslySetInnerHTML={{
                                    __html: activeCurriculum?.description as unknown as string ?? ''
                                }}></div>: 
                                <div className='w-full h-[400px] flex items-center justify-center'>
                                    <div className='flex flex-col items-center space-y-4'>
                                        <HiOutlineLockClosed className='w-12 h-12' />
                                        <div>Materi Masih Terkunci.</div>
                                        <Button onClick={() => {
                                            if (['incubation'].includes(registrant?.status ?? '')) {
                                                dispatch(bannerActions.addBannerData(`${app.APP_URL}/templates/v1/landing/lock-main-class.png`))
                                            }
                                            dispatch(bannerActions.addBanner("lock_curriculum"))
                                        }}>
                                            <div className='flex items-center space-x-2'>
                                                <HiOutlineLockOpen />
                                                <div>Buka Materi</div>
                                            </div>
                                        </Button>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    {
                        activeCurriculum &&
                        <div className="flex flex-col gap-5">
                            <div className="flex flex-row gap-2 h-[60px] px-7 border-b border-b-[#dbdbdb] overflow-x-auto whitespace-nowrap sticky top-[70px] bg-white z-30 small-scroll text-[0.8rem]">
                                <TabMenu path="task" text="Tugas" />
                                {/* <TabMenu path="main" text="Materi Utama" /> */}
                                {
                                    categoryAccessible &&
                                    <TabMenu path="other" text="Materi Lainnya" />
                                }
                                {/* <TabMenu path="mentor" text="Kontak Mentor" /> */}
                                {/* <TabMenu path="forum" text="Forum" /> */}
                                {/* <TabMenu path="complain" text="Keluhan" /> */}
                                <TabMenu path="reviews" text="Ulasan" />
                            </div>
                            <div className="bg-white">
                                <Outlet />
                            </div>
                        </div>
                    }
                </div>
                <div className="hidden md:flex flex-col w-1/4">
                    <ClassSection />
                </div>
            </div>
        </>
    )
}
