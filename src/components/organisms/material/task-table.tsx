import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '@/src/store';
import { classById, classCategoryById, classCurriculumById } from '@/src/store/classSlice';
import { HiChevronRight, HiOutlineEye, HiOutlineInformationCircle, HiXMark } from 'react-icons/hi2';
import { createPortal } from 'react-dom';
import { TaskType } from '@/src/store/types/RegistrantTypes';
import { toast } from 'react-toastify';
import { getRegistrantTaskUnfinished, registrantLoading } from '@/src/store/registrantSlice';
import { submitTaskApi } from '@/src/api/registrantApi';
import Loader from '../../all/loader';
import moment from 'moment';

interface ShowTaskModalProps {
  modalDismissed: boolean;
  setModalDismissed: React.Dispatch<React.SetStateAction<boolean>>;
  onConfirm?: React.MouseEventHandler
  task: TaskType
}

function ShowTaskModal({modalDismissed,setModalDismissed,onConfirm,task}: ShowTaskModalProps) {
  const dispatch: AppDispatch = useDispatch()
  const curriculum = useSelector((state: RootState) => classCurriculumById(state, task.curriculum_id as unknown as number))
  const curriculumCategory = useSelector((state: RootState) => classCategoryById(state, curriculum?.curriculum_category_id as unknown as number))
  const submitLoading = useSelector(registrantLoading).includes("submit_task")
  const [answer,setAnswer] = React.useState('')

  const _handleSubmitTask = () => {
      dispatch(submitTaskApi({
          payload: {
              answer: answer,
              task_id: task.id,
              registrant_id: task.registrant_id as unknown as number
          }
      })).then(resp => {
          toast("Task Submitted.", {
              type: 'success'
          })
      }).catch((err) => {
          toast("Error Submit!", {
              type: 'error'
          })
      })
  }
  
  return (
      <div className={`fixed inset-0 flex items-center justify-center z-[999] bg-black bg-opacity-50 ${modalDismissed ? 'hidden' : ''}`}>
          <div className="bg-white rounded-lg flex flex-col w-full md:max-w-[50vw]">
              <div className="flex flex-col gap-4 p-5">
                  <div className="flex flex-row gap-2 items-start justify-between">
                      <h1 className="text-neutral-90 text-xl font-medium space-y-3">
                          <div className="flex items-center space-x-2">
                              <div>{task.title}</div>
                          </div>
                          <div className="flex items-center space-x-2">
                              <div className="text-xs p-2 bg-gray-200 rounded-lg">{curriculumCategory?.title}</div>
                              <div>{'>'}</div>
                              <div className="text-xs p-2 bg-gray-200 rounded-lg">{curriculum?.title}</div>
                          </div>
                      </h1>
                      <button onClick={() => setModalDismissed(true)} className="bg-gray-100 hover:bg-gray-200 rounded-full p-2"><HiXMark /></button>
                  </div>
              </div>
              <div className="px-5"><hr /></div>
              <div className="p-5" dangerouslySetInnerHTML={{
                  __html: task.description ?? ''
              }} />
              <div className="px-5 pb-3">
                  <textarea name="" id="" rows={5} className="w-full resize-none border rounded p-4" placeholder="Tulis Jawaban" onChange={(e) => setAnswer(e.target.value)} value={answer}></textarea>
              </div>
              <button className={`px-4 py-3 text-white rounded-b-lg ${submitLoading ? 'bg-gray-300': 'bg-blue-600 hover:bg-blue-700'}`} disabled={submitLoading} onClick={() => _handleSubmitTask()}>
                  {
                      submitLoading &&
                      <Loader className="mr-2" />
                  }
                  Submit
              </button>
          </div>
      </div>
  )
}

function TaskItem ({item}: {item: TaskType}) {
  const [modalDismissed,setModalDismissed] = React.useState(true)
  const curriculum = useSelector((state:RootState) => classCurriculumById(state,item.curriculum_id as unknown as number))
  const curriculumCategory = useSelector((state:RootState) => classCategoryById(state,curriculum?.curriculum_category_id as unknown as number))
  return (
      <TableRow>
          <TableCell align={'center'}>
            <div className="flex flex-col items-start space-y-1">
                <div className="font-semibold text-sm">{item.title}</div>
                <div className="flex items-center space-x-1">
                    <div className="text-xs rounded-lg bg-gray-100 px-2 py-1 inline-block">{curriculumCategory?.title}</div>
                    <div>{">"}</div>
                    <div className="text-xs rounded-lg bg-gray-100 px-2 py-1 inline-block">{curriculum?.title}</div>
                </div>
            </div>
          </TableCell>
          <TableCell align={'center'}>
            {item.deadline_date ? moment(item.deadline_date).fromNow(): 'Tidak ada deadline.'}
          </TableCell>
          <TableCell align={'center'}>
            {
                  item.status == 'progress' &&
                  <div className="flex flex-row items-center justify-center gap-1 bg-[#FFE2E2] rounded-full px-3 py-1">
                      <HiXMark />
                      <span className="whitespace-nowrap">Belum dikerjakan</span>
                  </div>
              }
              {
                  item.status == 'done' &&
                  <div className="flex flex-row items-center justify-center gap-1 bg-yellow-300 rounded-full px-3 py-1">
                      <HiOutlineInformationCircle />
                      <span className="whitespace-nowrap">Sedang Diperiksa</span>
                  </div>
              }
          </TableCell>
          <TableCell align={'center'}>
            <>
                  {
                      item.status == 'progress' &&
                      <>
                          <div className="flex flex-row items-center justify-center gap-1 text-white bg-primary-main rounded-lg px-3 py-1 cursor-pointer" onClick={() => setModalDismissed(!modalDismissed)}>
                              <HiOutlineEye />
                              <span className="mr-2">Submit</span>
                              <HiChevronRight />
                              
                          </div>
                          {
                              !modalDismissed &&
                              createPortal(
                                  <ShowTaskModal task={item} modalDismissed={modalDismissed} setModalDismissed={setModalDismissed} />,
                                  document.body
                              )
                          }
                      </>
                  }
                  {
                      item.status == 'done' &&
                      <div>Sudah Dikirim</div>
                  }
              </>
          </TableCell>
      </TableRow>
  )
}

export default function TaskTable({tableOnly}:{tableOnly?: boolean}) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const {classId} = useParams()
  const currentClass = useSelector((state:RootState) => classById(state,classId as unknown as number))
  const unfinishedTasks = useSelector(getRegistrantTaskUnfinished)

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div className='space-y-6'>
      {
        !tableOnly &&
        <h1 className="font-bold text-lg px-6">Tugas {currentClass?.title}</h1>
      }
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <TableContainer sx={{ maxHeight: (tableOnly ? 'auto': 440) }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell
                  align={'center'}
                >
                  Judul Tugas
                </TableCell>
                <TableCell
                  align={'center'}
                >
                  Deadline
                </TableCell>
                <TableCell
                  align={'center'}
                >
                  Status
                </TableCell>
                <TableCell
                  align={'center'}
                >
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                  unfinishedTasks.sort((item1,item2) => {
                    if (item1 == null || item1.status == null) {
                      return 1; // item1 is null or has no status
                    }
                    if (item2 == null || item2.status == null) {
                      return -1; // item2 is null or has no status
                    }
                    return item2.status.localeCompare(item1.status);
                  }).map((item: TaskType) => (
                      <TaskItem item={item} key={item.id} />
                  ))
              }
              {
                  unfinishedTasks.length == 0 &&
                  <TableRow>
                      <TableCell colSpan={4}>Belum ada Tugas.</TableCell>
                  </TableRow>
              }
            </TableBody>
          </Table>
        </TableContainer>
        {/* <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        /> */}
      </Paper>
    </div>
  );
}
