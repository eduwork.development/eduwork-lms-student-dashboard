import { useDispatch, useSelector } from 'react-redux';
import LeftSidebar from '../molecules/left-sidebar'
import Topbar from '../molecules/topbar'
import { ReactNode } from 'react'
import { bannerActions, selectBanner } from '@/src/store/bannerSlice';
import { createPortal } from 'react-dom';
import { app } from '@/src/config/app';
import Modal, { ModalContentOnly } from '../molecules/modal';
import { AppDispatch } from '@/src/store';

const BannerProvider = ({children}: {children: ReactNode}) => {
    const lockCurriculumBanner = useSelector(selectBanner.opens).includes("lock_curriculum")
    const bannerdata = useSelector(selectBanner.data)
    const dispatch: AppDispatch = useDispatch()
    return (
        <>
            {children}
            {
                lockCurriculumBanner &&
                createPortal(
                    <ModalContentOnly isDismissed={!lockCurriculumBanner} setIsDismissed={() => dispatch(bannerActions.removeBanner("lock_curriculum"))}>
                        <a href={`${app.APP_URL}/joinkonsul`} target="_blank">
                            <img src={`${bannerdata ? bannerdata: app.APP_URL+'/templates/v1/landing/lock-konsul.jpg'}`} alt="Error load image."
                                srcSet="" className="banner-lock rounded-lg" />
                        </a>
                    </ModalContentOnly>,
                    document.body
                )
            }
        </>
    )
}

interface StudentTemplateDefaultProps {
    children?: ReactNode;
    title?: string;
}
interface StudentTemplateClassChapterProps {
    children?: ReactNode;
}

const Default = function ({ children, title }: StudentTemplateDefaultProps) {
    return (
        <BannerProvider>
            <div className="flex flex-row items-stretch max-h-screen w-full">
                {/* left-sidebar */}
                <LeftSidebar />

                {/* main content */}
                <div className="w-full overflow-y-scroll">
                    {/* topbar */}
                    <Topbar.Default title={title} className="sticky top-0 w-full z-[100]" />

                    {/* main content */}
                    <div>
                        {children}
                    </div>
                </div>
            </div>
        </BannerProvider>
    )
}

const ClassChapter = function ({ children }: StudentTemplateClassChapterProps) {
    
    return (
        <BannerProvider>
            <div className="flex flex-row items-stretch max-h-screen w-full">
                {/* left-sidebar */}
                <LeftSidebar />

                {/* main content */}
                <div className="w-full overflow-y-scroll">
                    {/* topbar */}
                    <Topbar.ClassChapter className="sticky top-0 w-full z-[100]" />

                    {/* main content */}
                    <div>
                        {children}
                    </div>
                </div>
            </div>
        </BannerProvider>
    )
}

const StudentTemplate = {
    Default, ClassChapter
}

export default StudentTemplate
