import { HiBookOpen, HiCheck, HiChevronDown, HiChevronUp, HiOutlineClock, HiOutlineExclamationCircle, HiOutlineLockClosed, HiOutlineLockOpen, HiXMark } from "react-icons/hi2"
import { useDispatch, useSelector } from "react-redux";
import { setActiveCollapse } from "../../store/collapseSlice";
import { AppDispatch, RootState } from "../../store";
import { CategoryType, CurriculumType } from "@/src/store/types/ClassTypes";
import { classActions, classActiveCurriculum, classCategoryById, classCurriculumByCategoryId, classCurriculumById, classTaskByCurriculumId } from "@/src/store/classSlice";
import Skeleton from "./skeleton";
import { StatusType, useGetCurriculumStatus } from "@/src/store/hooks/class-hooks";
import { canAccessCurriculum, getRegistrantByClassId, getRegistrantLastProgress, getRegistrantTaskAll, registrantLoading, selectRegistrantCategoryStatus, selectRegistrantCurriculumStatus, selectRegistrantProgressByCurriculumId, selectRegistrantTaskAllByCategoryId, selectRegistrantTaskAllByCurriculumId, selectRegistrantTaskFinishedByCurriculumId, selectRegistrantTaskResolvedByCurriculumId, selectRegistrantTaskUnresolvedByCurriculumId } from '@/src/store/registrantSlice';
import { useNavigate, useParams } from "react-router-dom";
import { Tooltip } from "@mui/material";
import { memo, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { openCurrentProgressApi } from "@/src/api/registrantApi";


interface CollapseChildProps {
    status: StatusType
    curriculumData: CurriculumType
}
interface CollapseProps {
    categoryData: CategoryType
}

const CollapseChild = memo(({ status, curriculumData }: CollapseChildProps) => {
  const activeCurriculum: number | null = useSelector(classActiveCurriculum)
  const dispatch: AppDispatch = useDispatch()
  const web = useNavigate()
  const {classId} = useParams()
  const registrant = useSelector((state: RootState) => getRegistrantByClassId(state,classId as unknown as number))
  const curriculumCategoryAccessible = useSelector((state:RootState) => canAccessCurriculum(state, {curriculumCategoryId:curriculumData.curriculum_category_id as unknown as number,registrantId: registrant?.id as unknown as number}))
  const currentProgress = useSelector((state: RootState) => selectRegistrantProgressByCurriculumId(state, curriculumData.id as unknown as number))

  return (
      <div onClick={() => {
          if (status == 'unlocked') {
            dispatch(classActions.setActiveCurriculum(curriculumData.id))
            web(`/my-class/${classId}/material/${curriculumData.id}/task`)
          } else if (status != 'locked') {
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth"
            });
          }
          if (status != 'locked' || !curriculumCategoryAccessible || (curriculumCategoryAccessible && currentProgress)) {
            dispatch(classActions.setActiveCurriculum(curriculumData.id))
            web(`/my-class/${classId}/material/${curriculumData.id}/task`)
          }
      }} className={`flex flex-row items-center gap-3 px-3 py-2 border bg-white ${status == 'locked' ? '!bg-neutral-20' : ''} ${(curriculumCategoryAccessible && currentProgress || status == 'unlocked') ? 'cursor-pointer': '!cursor-not-allowed'} ${activeCurriculum == curriculumData.id ? '!bg-blue-100':''} hover:bg-blue-100`} >
          {
            status == 'finished' ? <div className="text-[#0C8048]"><HiCheck /></div>: 
              (status == null ? <div className="text-[#A4A4A4]"><HiBookOpen /></div>:
              //  (curriculumCategoryAccessible && currentProgress) ? <div className="text-[#A4A4A4]"><HiOutlineLockClosed /></div> :
                (status == 'waiting' ? <div className="text-white rounded-lg bg-yellow-600 p-0.5"><HiOutlineClock /></div> : 
                  (status == 'unfinished' ? <div className="text-white rounded-lg bg-danger-main p-0.5"><HiXMark /></div> : 
                    (status == 'unlocked' ? <div className="rounded-lg p-0.5"><HiOutlineLockOpen /></div> : 
                      (status == 'ongoing' ? <div className="rounded-lg p-0.5"><HiOutlineLockOpen /></div> : 
                        (status == 'locked' ? <div className="text-[#A4A4A4]"><HiOutlineLockClosed /></div> : <div className="text-[#0C8048]"><Skeleton className="h-4 w-4 rounded-lg" /></div>))))))
          }
          <h4 className={status == 'locked' ? 'text-[#A9A9A9]' : 'text-text-heading'}>{curriculumData.title}</h4>
      </div>
  )
})

const CollapseItem = memo(({curriculum,accessible}: {curriculum: CurriculumType,accessible: boolean}) => {
  const lastProgress = useSelector(getRegistrantLastProgress)
  const lastProgressCurriculum = useSelector((state:RootState) => classCurriculumById(state, lastProgress?.curriculum_id as unknown as number))

  const progressStatus = useSelector((state: RootState) => selectRegistrantCurriculumStatus(state,{
    activeCurriculum: lastProgressCurriculum,
    curriculumId: curriculum.id
  }))
  const [tooltipText,setTooltipText] = useState<string>('')

  useEffect(() => {
    if (progressStatus == 'unfinished') {
      setTooltipText("Ada task yang belum dikerjakan.")
    } else if (progressStatus == 'waiting') {
      setTooltipText("Menunggu dicek mentor.")
    } else if (progressStatus == 'locked') {
      setTooltipText("Materi masih terkunci.")
    } else {
      setTooltipText("")
    }
  }, [progressStatus])

  return (
    <>
      {
        tooltipText ?
        <Tooltip title={tooltipText} placement="left" arrow>
          <div>
            <CollapseChild status={(accessible ? progressStatus ?? null: 'locked')} curriculumData={curriculum} />
          </div>
        </Tooltip>:
        <CollapseChild status={(accessible ? progressStatus ?? null: 'locked')} curriculumData={curriculum} />
      }
    </>
  )
})

function Collapse({ categoryData }: CollapseProps) {
    const dispatch = useDispatch()
    const {classId} = useParams()
    const registrant = useSelector((state:RootState) => getRegistrantByClassId(state,classId as unknown as number))
    const activeCollapse = useSelector((state: RootState) => state.collapseState.activeCollapse)
    const curriculumData = useSelector((state: RootState) => classCurriculumByCategoryId(state,categoryData.id))

    const lastProgress = useSelector(getRegistrantLastProgress)
    const activeCurriculum = useSelector((state:RootState) => classCurriculumById(state, lastProgress?.curriculum_id as unknown as number))
    const curriculumStatusses = useSelector((state: RootState) => selectRegistrantCategoryStatus(state, {
      categoryId: categoryData.id,
      activeCurriculum
    }))
    const curriculumCategoryAccessible = useSelector((state:RootState) => canAccessCurriculum(state, {curriculumCategoryId:categoryData.id as unknown as number,registrantId: registrant?.id as unknown as number}))
    return (
        <div className="flex flex-col">
            <button onClick={() => {
                dispatch(setActiveCollapse(categoryData.id))
            }} className={`flex flex-row items-center bg-primary-surface border border-neutral-40 p-3`}>
                {
                  !curriculumCategoryAccessible ? <div className="text-[#A4A4A4]"><HiOutlineLockClosed /></div>:
                  (
                    curriculumStatusses.includes("waiting") || curriculumStatusses.includes("unfinished") ? <div className="text-white rounded-lg bg-yellow-600 p-0.5"><HiOutlineClock /></div> : 
                  ( curriculumStatusses.length == 0 || (!curriculumStatusses.includes("waiting") && !curriculumStatusses.includes("unfinished") && !curriculumStatusses.includes("finished")) ? (curriculumStatusses.includes(null) ? <div className="rounded-lg p-0.5 text-[#A4A4A4]"><HiBookOpen /></div>:<div className="text-[#A4A4A4]"><HiOutlineLockClosed /></div>):
                  ( !curriculumStatusses.includes("waiting") && !curriculumStatusses.includes("unfinished") && curriculumStatusses.includes("finished") && curriculumStatusses.length > 0 ? <div className="text-white rounded-lg bg-green-600 p-0.5"><HiCheck /></div>:<div className="rounded-lg p-0.5"></div>))
                  )
                }
                <div className="flex flex-col gap-1 ml-3 mr-auto items-start">
                    <h3 className="font-bold text-left">{categoryData.title}</h3>
                    <span className="text-xs">0/{curriculumData.length} | 28 menit</span>
                </div>
                {activeCollapse != categoryData.id ? <HiChevronDown /> : <HiChevronUp />}
            </button>
            <div className={`flex flex-col transition-all duration-500 overflow-y-hidden ${activeCollapse == categoryData.id ? 'max-h-[1000px]' : 'max-h-[0]'}`}>
                {curriculumData.map((curriculum: CurriculumType) => {
                    return <CollapseItem key={curriculum.id} curriculum={curriculum} accessible={curriculumCategoryAccessible} />
                })}
            </div>
        </div>
    )
}

export default memo(Collapse)