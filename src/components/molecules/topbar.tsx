import Dropdown from "../atoms/dropdown"
import TabMenu from "../atoms/tab-menu"
import { toggleSidebar } from "../../store/sidebarSlice"
import { useDispatch, useSelector } from 'react-redux'
import { HiOutlineBell } from "react-icons/hi2"
import { useLocation, useNavigate, useParams } from "react-router-dom"
import { RootState } from "@/src/store"
import { useEffect, useLayoutEffect, useState } from "react"
import { getRegistrantLastProgress } from "@/src/store/registrantSlice"
import { classActions } from "@/src/store/classSlice"

interface TopbarDefaultProps {
    title?: string;
    className?: string;
}
interface TopbarClassChapterProps {
    className?: string;
}

const Default = function ({ title, className }: TopbarDefaultProps) {
    const dispatch = useDispatch()

    return (
        <div className={`h-[70px] px-4 bg-white flex flex-row gap-4 items-center shadow ${className}`}>
            <button onClick={() => dispatch(toggleSidebar())}><i className="bi bi-list text-xl font-bold"></i></button>
            <h1 className="text-xl font-bold">{title}</h1>
            <HiOutlineBell className={`ml-auto text-xl`} />
        </div>
    )
}

const ClassChapter = function ({ className }: TopbarClassChapterProps) {
    const {classId,curriculumId} = useParams()
    const dispatch = useDispatch()
    const [vClassId,setVClassId] = useState('')
    const web = useNavigate()
    const location = useLocation()
    const myClasses = useSelector((state: RootState) => state.classState.class)
    const activeNav = useSelector((state: RootState) => state.classState.activeNav)
    const lastProgress = useSelector(getRegistrantLastProgress)
    const curriculumIdFromLastProgress = lastProgress?.curriculum_id

    const _handleChangeClass = (id: number) => {
        web(`/my-class/${id}/material/task`)
    }

    useLayoutEffect(() => {
      const pagePattern = /\/my-class\/(\d+(\.\d+)?(e\d+)?)(\/\w+)?/;
      let pageMatch = location.pathname.match(pagePattern)
      if (pageMatch) {
        const [, decimalValue, , , result,] = pageMatch;
        if (result) {
          // If result exists, use it as the state
          dispatch(classActions.setActiveNav(result.slice(1)))
        } else {
          // If there's no result, use decimalValue as the state
          dispatch(classActions.setActiveNav('material'))
        }
      }
      if (location.pathname.endsWith("/material/main")) {
        if (curriculumIdFromLastProgress && (!curriculumId || Number.isNaN(parseInt(curriculumId ?? '')))) {
          web(`/my-class/${classId}/material/${curriculumIdFromLastProgress}/task`)
        }
      }
    }, [location])

    // useEffect(() => {
      
    // }, [curriculumIdFromLastProgress])
    
    useEffect(() => {
      setVClassId(classId ?? '')
    }, [])
    
    return (
        <div className={`h-[70px] px-4 bg-white flex flex-row gap-4 items-center text-[0.8rem] shadow ${className}`}>
            <button onClick={() => dispatch(toggleSidebar())}><i className="bi bi-list text-xl font-bold"></i></button>
            <Dropdown onChange={({target}: {target: HTMLSelectElement}) => _handleChangeClass(target.value as unknown as number)} defaultValue={vClassId}>
                <option value="" disabled></option>
                {
                    myClasses.map(item => <option key={item.id} value={item.id}>{item.title}</option>)
                }
            </Dropdown>
            <div className="flex flex-row h-full gap-2">
                <TabMenu path="material/main" text="Material" isActive={activeNav == 'material'} />
                <TabMenu path="task" text="All Task" isActive={activeNav == 'task'} />
            </div>
            <HiOutlineBell className={`ml-auto text-xl`} />
        </div>
    )
}

const Topbar = {
    Default, ClassChapter
}

export default Topbar