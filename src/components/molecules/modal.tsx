import { SetStateAction, MouseEventHandler, Dispatch, ReactNode } from 'react'
import { HiCheckCircle, HiExclamationCircle, HiInformationCircle, HiXMark } from "react-icons/hi2";
import Button from '../atoms/button';
import { LoaderPulse } from '../all/loader';

interface ModalContentOnlyProps {
    isDismissed: boolean;
    setIsDismissed: Function;
    type?: 'danger' | 'success' | 'warning' | 'info';
    title?: string;
    body?: string | ReactNode;
    confirmBtnText?: string | ReactNode;
    cancelBtnText?: string;
    onConfirm?: MouseEventHandler;
    buttonLoading?: boolean;
    children?: string | ReactNode
}

export function ModalContentOnly({ isDismissed, setIsDismissed, buttonLoading = false, type, title, body, confirmBtnText = 'Confirm', cancelBtnText = 'Cancel', onConfirm = () => { }, children}: ModalContentOnlyProps) {
    return (
        <div className={`fixed inset-0 flex items-center justify-center z-[999] bg-black bg-opacity-50 ${isDismissed ? 'hidden' : ''}`}>
            <div className="bg-white rounded-lg flex flex-col md:max-w-[80vw] md:max-h-[80vh] relative">
                <button className='p-2 rounded-full bg-gray-100 hover:bg-gray-200 absolute top-[1rem] right-[1rem]' onClick={() => setIsDismissed()}><HiXMark className='w-6 h-6' /></button>
                <div className="flex flex-col">
                    <div className="flex flex-row items-center gap-2">
                        {type == 'danger' &&
                            <span className="text-danger-main text-3xl"><HiExclamationCircle /></span>
                        }
                        {type == 'success' &&
                            <span className="text-success-main text-3xl"><HiCheckCircle /></span>
                        }
                        {type == 'warning' &&
                            <span className="text-secondary-main text-3xl"><HiExclamationCircle /></span>
                        }
                        {type == 'info' &&
                            <span className="text-[#17a2b8] text-3xl"><HiInformationCircle /></span>
                        }
                        <h1 className="text-neutral-90 text-xl font-medium">{title}</h1>
                    </div>
                    <div className="text-sm text-neutral-70">{body ?? children}</div>
                </div>
            </div>
        </div>
    )
}

interface ModalProps {
    isDismissed: boolean;
    setIsDismissed: Dispatch<SetStateAction<boolean>>;
    type?: 'danger' | 'success' | 'warning' | 'info';
    title?: string;
    body?: string | ReactNode;
    confirmBtnText?: string | ReactNode;
    cancelBtnText?: string;
    onConfirm?: MouseEventHandler;
    buttonLoading?: boolean;
}

export default function Modal({ isDismissed, setIsDismissed, buttonLoading = false, type, title, body, confirmBtnText = 'Confirm', cancelBtnText = 'Cancel', onConfirm = () => { } }: ModalProps) {
    return (
        <div className={`fixed inset-0 flex items-center justify-center z-[999] bg-black bg-opacity-50 ${isDismissed ? 'hidden' : ''}`}>
            <div className="bg-white rounded-lg flex flex-col w-full md:w-1/3">
                <div className="flex flex-col gap-3 p-5">
                    <div className="flex flex-row items-center gap-2">
                        {type == 'danger' &&
                            <span className="text-danger-main text-3xl"><HiExclamationCircle /></span>
                        }
                        {type == 'success' &&
                            <span className="text-success-main text-3xl"><HiCheckCircle /></span>
                        }
                        {type == 'warning' &&
                            <span className="text-secondary-main text-3xl"><HiExclamationCircle /></span>
                        }
                        {type == 'info' &&
                            <span className="text-[#17a2b8] text-3xl"><HiInformationCircle /></span>
                        }
                        <h1 className="text-neutral-90 text-xl font-medium">{title}</h1>
                    </div>
                    <div className="text-sm text-neutral-70">{body}</div>
                </div>
                {
                    ((cancelBtnText || confirmBtnText) && !buttonLoading) &&
                    <div className="bg-neutral-20 flex flex-col items-end p-3 rounded-b-lg">
                        <div className="flex flex-row items-center gap-2">
                            <Button text={cancelBtnText} type='light' borderRadius='lg' size='small' onClick={() => setIsDismissed(true)} />
                            <Button text={confirmBtnText} type={type} borderRadius='lg' size='small' onClick={onConfirm} />
                        </div>
                    </div>
                }
                {
                    buttonLoading &&
                    <div className="bg-neutral-20 flex flex-col items-end p-3 rounded-b-lg">
                        <div className="flex flex-row items-center gap-2">
                            <Button text={<div className="my-1"><LoaderPulse color='loader-light' /></div>} type={"light"} borderRadius='lg' size='small' onClick={onConfirm} />
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}
