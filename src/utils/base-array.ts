export {}

declare global {
  interface Array<T> {
    first(): T | undefined; // Define the `first` method for arrays
    last(): T | undefined;  // Define the `last` method for arrays
  }
}

// Implement the custom methods
if (!Array.prototype.first) {
  Array.prototype.first = function<T>() {
    if (this.length === 0) {
      return undefined;
    }
    return this[0];
  };
}

if (!Array.prototype.last) {
  Array.prototype.last = function<T>() {
    if (this.length === 0) {
      return undefined;
    }
    return this[this.length - 1];
  };
}